from __future__ import print_function
from collections import namedtuple
from enum import Enum
from pprint import pprint
import json
import sys


class Section(Enum):
    REMOVE = "should remove these lines"
    ADD = "should add these lines"
    FULL = "full include-list"


new_suggestion = lambda: {"to_add": [], "to_remove": []}
SUGGESTION_START = "Warning: include-what-you-use"
SUGGESTION_END = "---"

def parse_iwyu_out(filename):
    """ Returns { path: {to_remove:[lines],
                         to_add:[lines]}}
    """
    suggestions = {}
    cur_suggestion = None
    cur_section = None
    name = None

    with open(filename, "r") as f:
        for line in f :
            line = line.strip().replace("\n", "")
            if not line: continue

            if not cur_suggestion:
                if SUGGESTION_START in line:
                    cur_suggestion = new_suggestion()
                else:
                    for s in Section:
                        if s.value in line:
                            cur_suggestion = new_suggestion()
                            cur_section = s
            else:
                if SUGGESTION_END in line:
                    suggestions[name] = cur_suggestion
                    cur_suggestion = None
                    cur_section = None
                    continue

                starting_section = False
                for s in Section:
                    if s.value in line:
                        cur_section = s
                        if s == Section.FULL:
                            name = line.split()[-1].strip(":")
                        starting_section = True
                if starting_section: continue

                if cur_section == Section.REMOVE:
                    cur_suggestion["to_remove"].append(line)
                elif cur_section == Section.ADD:
                    cur_suggestion["to_add"].append(line)
    return suggestions


if __name__ == "__main__":
    filename = sys.argv[1]
    parsed_suggestions = parse_iwyu_out(filename)

    with open("out.json", "w") as f:
        json.dump(parsed_suggestions, f)
