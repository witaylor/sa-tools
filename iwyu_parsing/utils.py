import json
from collections import defaultdict


def fname(path):
    return path.split("/")[-1]

def ext(fn):
    parts = fn.split(".")
    return parts[-1] if len(parts) > 1 else None

def path_to_package(path):
    path_parts = path.lstrip("/scratch/witaylor/athena").split("/")
    return "/".join((sub for sub in path_parts if sub not in ["src", "test"]))

def map_keys(fn, d):
    return {fn(k): v for k, v in d.items()}

def fname_occurences(d):
    """ returns { fname: [fpaths] } """
    file_paths = defaultdict(list)
    for k, v in d.items():
        file_paths[fname(k)].append(k)
    return file_paths

def fnames(d):
    return set((fname(k) for k in d))

def sugg_amount(k, sugg):
    return len(sugg[1][k])

def most_changes_suggested(suggestions, quantity="total", n=10):
    if quantity == "total":
        k = lambda s: sugg_amount("to_remove", s) \
                    + sugg_amount("to_add", s)
    else:
        k = lambda s: sugg_amount(quantity, s)

    return sorted(suggestions.items(),
                  key=k,
                  reverse=True)[:min(n, len(suggestions))]
