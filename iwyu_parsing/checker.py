import json
import re


with open("out.json", "r") as f:
    data = json.load(f)

valids = [re.compile("^(- )?#include .+")]
valids.extend([
    re.compile("^(- )?{} .+;".format(s)) for s in ("class", "template", "struct", "namespace")
])

invalids = []
for d in data.values():
    for line in d["to_add"] + d["to_remove"]:
        if not any((check.match(line) for check in valids)):
            invalids.append(line)

print("{} invalid lines were included".format(len(invalids)))
if invalids:
    print("Examples:\n{}".format(invalids[:min(len(invalids), 25)]))
