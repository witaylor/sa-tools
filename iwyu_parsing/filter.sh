#!/bin/bash

awk '!/\[.+\]/ && !/^(Warning|  Possible|  Extraction|Scanning|Detaching)/ && !/.+has correct.+/' iwyu_out | sed '/^$/d' > filtered_iwyu_out;
