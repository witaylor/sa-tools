from __future__ import print_function
import parser
from utils import *

DEFAULT_IWYU = "filtered_iwyu_out"
DEFAULT_TIMES = "ninja_times"

suggestions = parser.parse_iwyu_out(DEFAULT_IWYU)

rep_out_fs = {k: v for k, v in fname_occurences(suggestions).items() if len(v) > 1}

ks = ["to_remove", "to_add", "total"]

for k in ks:
    most = most_changes_suggested(suggestions, quantity=k)
    print("Files with the most suggestions {}:".format(k))
    for s in most:
        to_remove = sugg_amount("to_remove",  s)
        to_add = sugg_amount("to_add", s)
        print("{}, (-{} +{}, total {})".format(
            s[0], to_remove, to_add, to_remove + to_add))
    print()
