from __future__ import print_function
import os
import subprocess
import re
import json
import argparse
from collections import defaultdict


include_uses_filename = "include_uses"
output_filename = "includes.json"

def find_shared_py_files(base_path):
    """ Gets all python files with a 'share' directory as an ancestor """
    shared_py_files = []
    for dirpath, dnames, fnames in os.walk(base_path):
        if "share" in dirpath.split(os.sep):
            shared_py_files.extend((f for f in fnames if f.endswith(".py")))
    return shared_py_files

def find_include_uses(base_path):
    with open(include_uses_filename, "w") as f:
        subprocess.call(["grep", "-rP", "--include=*.py",
                         "include\(.+\)", base_path], stdout=f)

def build_include_uses():
    """ Outputs all uses of the 'include' function with a string literal to a file """
    includes = defaultdict(list)
    # Gets the string inside the call to include.
    # Note: this will avoid includes where the filename is a variable.
    included_file = re.compile("include\(\s*[\"'](.+)[\"']\s*\)")
    with open(include_uses_filename, "r") as f:
        for line in f:

            if line.startswith("#"): continue

            split_line = line.split(":")
            path, grep_match = split_line[0], "".join(split_line[1:])
            file_match = included_file.match(grep_match)
            if file_match:
                includes[path].append(file_match.groups()[0].strip(" '\""))
    return includes

def list_all_missing(includes, include_targets):
    """ Prints the path of every file which only includes files which do not exist in include_targets """
    for filepath, includes in includes.items():
        simple_includes = [i.split(os.sep)[-1] for i in includes]
        impossible_includes = [i for i in simple_includes if i not in include_targets]
        if len(impossible_includes) == len(includes):
            print(filepath)

def summary(includes, include_targets):
    num_files_with_missing_includes = 0
    total_files = len(includes.keys())
    all_missing = 0
    for filepath, includes in includes.items():
        simple_includes = [i.split(os.sep)[-1] for i in includes]
        impossible_includes = [i for i in simple_includes if i not in include_targets]
        if impossible_includes:
            num_files_with_missing_includes += 1
            if len(impossible_includes) == len(includes):
                all_missing += 1
            print("{}/{} includes in {} do not exist".format(len(impossible_includes), len(includes), filepath))
    print("{}/{} files have missing includes".format(num_files_with_missing_includes, total_files))
    print("{} of these ONLY have missing includes".format(all_missing))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--path", required=False, default="/scratch/witaylor/athena")
    parser.add_argument("-r", "--rebuild", action='store_true')
    parser.add_argument("-l", "--list-all-missing", action='store_true')
    args = parser.parse_args()

    shared_py_files = find_shared_py_files(args.path)
    if not os.path.exists(include_uses_filename) or args.rebuild:
        find_include_uses(args.path)
    includes = build_include_uses()
    with open("includes.json", "w") as f:
        json.dump({"include_targets": shared_py_files, "includes": includes}, f)

    if args.list_all_missing:
        list_all_missing(includes, shared_py_files)
    else:
        summary(includes, shared_py_files)
